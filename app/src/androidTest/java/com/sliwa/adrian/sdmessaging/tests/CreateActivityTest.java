package com.sliwa.adrian.sdmessaging.tests;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.view.ContextThemeWrapper;

import com.sliwa.adrian.sdmessaging.app.CreateActivity;
import com.sliwa.adrian.sdmessaging.app.R;
import com.sliwa.adrian.sdmessaging.app.SettingsActivity;
import com.sliwa.adrian.sdmessaging.rest.RestAPI;
import com.sliwa.adrian.sdmessaging.rest.RestAPICallback;
import com.sliwa.adrian.sdmessaging.tests.tools.TestMenuItem;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import common.sdm.message.Message;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Adiki on 2014-06-14.
 */
public class CreateActivityTest extends ActivityUnitTestCase<CreateActivity> {

    private Intent launchIntent;

    public CreateActivityTest() {
        super(CreateActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ContextThemeWrapper context = new ContextThemeWrapper(getInstrumentation().getTargetContext(), R.style.AppTheme);
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
        setActivityContext(context);
        launchIntent = new Intent(getInstrumentation()
                .getTargetContext(), CreateActivity.class);
        startActivity(launchIntent, null, null);
    }

    public void testPreconditions() {
        assertNotNull(launchIntent);
    }

    public void testOnOptionsItemSelectedSettings() {
        getActivity().onOptionsItemSelected(new TestMenuItem() {
            @Override
            public int getItemId() {
                return R.id.action_settings;
            }
        });
        Intent intent = getStartedActivityIntent();
        assertNotNull(intent);
        assertEquals(SettingsActivity.class.getName(), intent.resolveActivity(getActivity().getPackageManager()).getClassName());

    }

    public void testOnOptionsItemSelectedSendMessage() {
        RestAPI mockRestAPI = mock(RestAPI.class);
        getActivity().setRestAPI(mockRestAPI);
        getActivity().onOptionsItemSelected(new TestMenuItem() {
            @Override
            public int getItemId() {
                return R.id.action_send;
            }
        });
        verify(mockRestAPI, times(1)).sendMessage(any(Message.class), any(RestAPICallback.class));


    }

    public void testRestAPIMessageCallback() {
        RestAPI mockRestAPI = mock(RestAPI.class);
        getActivity().setRestAPI(mockRestAPI);
        doAnswer(
            new Answer<Object>() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    ((RestAPICallback)invocation.getArguments()[1]).onTaskComplete("OK");
                    return null;
                }
            }
        ).when(mockRestAPI).sendMessage(any(Message.class), any(RestAPICallback.class));
        getActivity().onOptionsItemSelected(new TestMenuItem() {
            @Override
            public int getItemId() {
                return R.id.action_send;
            }
        });
        assertTrue(isFinishCalled());
    }
}