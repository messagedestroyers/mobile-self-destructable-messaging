package com.sliwa.adrian.sdmessaging.tests;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.view.ContextThemeWrapper;

import com.sliwa.adrian.sdmessaging.adapters.MessageListAdapter;
import com.sliwa.adrian.sdmessaging.app.CreateActivity;
import com.sliwa.adrian.sdmessaging.app.MainActivity;
import com.sliwa.adrian.sdmessaging.app.MessageActivity;
import com.sliwa.adrian.sdmessaging.app.R;
import com.sliwa.adrian.sdmessaging.app.SettingsActivity;
import com.sliwa.adrian.sdmessaging.rest.MessagesCallback;
import com.sliwa.adrian.sdmessaging.rest.RestAPI;
import com.sliwa.adrian.sdmessaging.rest.RestAPICallback;
import com.sliwa.adrian.sdmessaging.tests.tools.TestMenuItem;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import common.sdm.message.Credentials;
import common.sdm.message.Message;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Adiki on 2014-06-14.
 */
public class MainActivityTest extends ActivityUnitTestCase<MainActivity> {

    private Intent launchIntent;
    private MainActivity.EvictionReceiver evictionReceiver;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ContextThemeWrapper context = new ContextThemeWrapper(getInstrumentation().getTargetContext(), R.style.AppTheme);
        setActivityContext(context);
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
        launchIntent = new Intent(getInstrumentation()
                .getTargetContext(), MainActivity.class);
        startActivity(launchIntent, null, null);
        evictionReceiver = getActivity().new EvictionReceiver();
        getActivity().setEvictionReceiver(evictionReceiver);
    }

    public void testPreconditions() {
        assertNotNull(launchIntent);
    }

}