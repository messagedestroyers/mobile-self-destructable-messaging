package com.sliwa.adrian.sdmessaging.tests;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.view.ContextThemeWrapper;

import com.sliwa.adrian.sdmessaging.app.CreateActivity;
import com.sliwa.adrian.sdmessaging.app.LoginActivity;
import com.sliwa.adrian.sdmessaging.app.MainActivity;
import com.sliwa.adrian.sdmessaging.app.R;
import com.sliwa.adrian.sdmessaging.app.SettingsActivity;
import com.sliwa.adrian.sdmessaging.rest.RestAPI;
import com.sliwa.adrian.sdmessaging.rest.RestAPICallback;
import com.sliwa.adrian.sdmessaging.tests.tools.TestMenuItem;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import common.sdm.message.Credentials;
import common.sdm.message.Message;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by Adiki on 2014-06-14.
 */
public class LoginActivityTest extends ActivityUnitTestCase<LoginActivity> {

    private Intent launchIntent;

    public LoginActivityTest() {
        super(LoginActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ContextThemeWrapper context = new ContextThemeWrapper(getInstrumentation().getTargetContext(), R.style.AppTheme);
        setActivityContext(context);
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
        launchIntent = new Intent(getInstrumentation()
                .getTargetContext(), LoginActivity.class);
        startActivity(launchIntent, null, null);
    }

    public void testPreconditions() {
        assertNotNull(launchIntent);
    }

    public void testOnOptionsItemSelectedSettings() {
        getActivity().onOptionsItemSelected(new TestMenuItem() {
            @Override
            public int getItemId() {
                return R.id.action_settings;
            }
        });
        Intent intent = getStartedActivityIntent();
        assertNotNull(intent);
        assertEquals(SettingsActivity.class.getName(), intent.resolveActivity(getActivity().getPackageManager()).getClassName());

    }

    public void testOnLoginSuccess() {
        RestAPI mockRestAPI = mock(RestAPI.class);
        getActivity().setRestAPI(mockRestAPI);
        doAnswer(
            new Answer<Object>() {
                @Override
                public Object answer(InvocationOnMock invocation) throws Throwable {
                    ((RestAPICallback)invocation.getArguments()[1]).onTaskComplete("OK");
                    return null;
                }
            }
        ).when(mockRestAPI).login(any(Credentials.class), any(RestAPICallback.class));
        getActivity().onLoginClick(null);
        Intent intent = getStartedActivityIntent();
        assertNotNull(intent);
        assertEquals(MainActivity.class.getName(), intent.resolveActivity(getActivity().getPackageManager()).getClassName());
    }

    public void testOnLoginFail() {
        RestAPI mockRestAPI = mock(RestAPI.class);
        getActivity().setRestAPI(mockRestAPI);
        doAnswer(
                new Answer<Object>() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        ((RestAPICallback)invocation.getArguments()[1]).onTaskComplete("FAIL");
                        return null;
                    }
                }
        ).when(mockRestAPI).login(any(Credentials.class), any(RestAPICallback.class));
        getActivity().onLoginClick(null);
        Intent intent = getStartedActivityIntent();
        assertNull(intent);
    }

    public void testOnRegistrationSuccess() {
        RestAPI mockRestAPI = mock(RestAPI.class);
        getActivity().setRestAPI(mockRestAPI);
        doAnswer(
                new Answer<Object>() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        ((RestAPICallback)invocation.getArguments()[1]).onTaskComplete("OK");
                        return null;
                    }
                }
        ).when(mockRestAPI).register(any(Credentials.class), any(RestAPICallback.class));
        getActivity().onRegisterClick(null);
        verify(mockRestAPI, times(1)).login(any(Credentials.class), any(RestAPICallback.class));
    }

    public void testOnRegistrationFail() {
        RestAPI mockRestAPI = mock(RestAPI.class);
        getActivity().setRestAPI(mockRestAPI);
        doAnswer(
                new Answer<Object>() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        ((RestAPICallback)invocation.getArguments()[1]).onTaskComplete(null);
                        return null;
                    }
                }
        ).when(mockRestAPI).register(any(Credentials.class), any(RestAPICallback.class));
        getActivity().onRegisterClick(null);
        verify(mockRestAPI, never()).login(any(Credentials.class), any(RestAPICallback.class));
    }

    public void testOnRegistrationUserExist() {
        RestAPI mockRestAPI = mock(RestAPI.class);
        getActivity().setRestAPI(mockRestAPI);
        doAnswer(
                new Answer<Object>() {
                    @Override
                    public Object answer(InvocationOnMock invocation) throws Throwable {
                        ((RestAPICallback)invocation.getArguments()[1]).onTaskComplete("User exist!");
                        return null;
                    }
                }
        ).when(mockRestAPI).register(any(Credentials.class), any(RestAPICallback.class));
        getActivity().onRegisterClick(null);
        verify(mockRestAPI, never()).login(any(Credentials.class), any(RestAPICallback.class));
    }
}