package com.sliwa.adrian.sdmessaging.tests;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.view.ContextThemeWrapper;

import com.sliwa.adrian.sdmessaging.app.MainActivity;
import com.sliwa.adrian.sdmessaging.app.MessageActivity;
import com.sliwa.adrian.sdmessaging.app.R;

import common.sdm.message.Message;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Adiki on 2014-06-14.
 */
public class MessageActivityTest extends ActivityUnitTestCase<MessageActivity> {

    private Intent launchIntent;
    private Message mockMessage;
    private MessageActivity.EvictionReceiver evictionReceiver;

    public MessageActivityTest() {
        super(MessageActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ContextThemeWrapper context = new ContextThemeWrapper(getInstrumentation().getTargetContext(), R.style.AppTheme);
        setActivityContext(context);
        System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
        launchIntent = new Intent(getInstrumentation()
                .getTargetContext(), MessageActivity.class);
        mockMessage = mock(Message.class);
        launchIntent.putExtra(MainActivity.MESSAGE, mockMessage);
        startActivity(launchIntent, null, null);
        evictionReceiver = getActivity().new EvictionReceiver();
        getActivity().setEvictionReceiver(evictionReceiver);
    }

    public void testPreconditions() {
        assertNotNull(launchIntent);
    }

    public void testFinishActivity() {
        when(mockMessage.getValidThroughInMs()).thenReturn(-1L);
        evictionReceiver.onReceive(null, null);
        assertTrue(isFinishCalled());
    }
}