package com.sliwa.adrian.sdmessaging.base;


import android.os.AsyncTask;

public class BackgroundTask<F, T> extends AsyncTask<F, Void, T> {

	private Function<F, T> function;
	private TaskCallback<T> taskCallback;

	public BackgroundTask(Function<F, T> function, TaskCallback<T> taskCallback) {
		this.function = function;
		this.taskCallback = taskCallback;
	}
	
	@Override
	protected T doInBackground(F... params) {
		return function.apply(params[0]);
	}
	
	@Override
	protected void onPostExecute(T result) {
        if (taskCallback != null) {
            taskCallback.onTaskComplete(result);
        }
	}

}
