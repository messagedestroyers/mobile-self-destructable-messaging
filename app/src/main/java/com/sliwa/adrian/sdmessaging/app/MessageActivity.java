package com.sliwa.adrian.sdmessaging.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import common.sdm.message.Message;


public class MessageActivity extends ActionBarActivity {
    private Message message;

    private BroadcastReceiver evictionReceiver = new EvictionReceiver();

    private TextView validThroughTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        message = (Message) getIntent().getSerializableExtra(MainActivity.MESSAGE);
        ((TextView)findViewById(R.id.fromTextView)).setText(message.getSender());
        ((TextView)findViewById(R.id.topicTextView)).setText(message.getTopic());
        ((TextView)findViewById(R.id.sentDateTextView)).setText(message.getSentDate());
        validThroughTextView = ((TextView)findViewById(R.id.validThroughTextView));
        validThroughTextView.setText(String.valueOf(message.getValidThroughInMin() + "m"));
        ((TextView)findViewById(R.id.messageTextView)).setText(message.getContent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(evictionReceiver, new IntentFilter("DUPA"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(evictionReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setEvictionReceiver(BroadcastReceiver evictionReceiver) {
        this.evictionReceiver = evictionReceiver;
    }

    public class EvictionReceiver extends BroadcastReceiver {

        private static final String TAG = "MessageActivity.EvictionReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Eviction performed ");
            validThroughTextView.setText(String.valueOf(message.getValidThroughInMin() + "m"));
            if (message.getValidThroughInMs() < 0) {
                MessageActivity.this.finish();
            }
        }
    }

}
