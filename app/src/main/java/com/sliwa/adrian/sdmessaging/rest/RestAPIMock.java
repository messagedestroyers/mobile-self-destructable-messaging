package com.sliwa.adrian.sdmessaging.rest;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.security.MessageDigestSpi;
import java.util.ArrayList;
import java.util.List;

import common.sdm.message.Message;

/**
 * Created by Adiki on 08.04.14.
 */
public class RestAPIMock extends RestAPI {
    public RestAPIMock(SharedPreferences sharedPreferences) {
        super(sharedPreferences);
    }

    @Override
    public void sendMessage(Message message, RestAPICallback restAPICallback) {
        restAPICallback.onTaskComplete("");
    }

    @Override
    public void getMessages(MessagesCallback messagesCallback) {
        Gson gson = new Gson();
        List<Message> list = new ArrayList<Message>();
        Message m = new Message("Adiki", "Niki", "Topic 1", "Content 1", null, null);
        list.add(m);
        m = new Message("Adiki", "Niki", "Topic 2", "Content 2", null, null);
        list.add(m);
        m = new Message("Adiki", "Niki", "Topic 2", "Content 2", null, null);
        list.add(m);
        m = new Message("Adiki", "Niki", "Topic 2", "Content 2", null, null);
        list.add(m);
        m = new Message("Adiki", "Niki", "Topic 2", "Content 2", null, null);
        list.add(m);
        m = new Message("Adiki", "Niki", "Topic 2", "Content 2", null, null);
        String json = gson.toJson(list);
        Type type = new TypeToken<List<Message>>(){}.getType();
        List<Message> messages = gson.fromJson(json, type);
        messagesCallback.onTaskComplete(messages);
    }
}
