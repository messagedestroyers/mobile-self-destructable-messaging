package com.sliwa.adrian.sdmessaging.rest;

import com.sliwa.adrian.sdmessaging.base.TaskCallback;

import java.util.List;

import common.sdm.message.Message;

/**
 * Created by Adiki on 09.04.14.
 */
public interface MessagesCallback extends TaskCallback<List<Message>> {
}
