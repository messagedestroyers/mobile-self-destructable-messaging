package com.sliwa.adrian.sdmessaging.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sliwa.adrian.sdmessaging.adapters.MessageListAdapter;
import com.sliwa.adrian.sdmessaging.rest.MessagesCallback;
import com.sliwa.adrian.sdmessaging.rest.RestAPI;

import java.lang.reflect.Field;
import java.util.List;

import common.sdm.message.Message;

public class MainActivity extends ActionBarActivity {
    private RestAPI restAPI;
    private ListView messageListView;

    private MessageListAdapter adapter;

    public final static String MESSAGE = "com.sliwa.adrian.sdmessaging.app.MESSAGE";
    private BroadcastReceiver evictionReceiver = new EvictionReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        messageListView = (ListView) findViewById(R.id.messageListView);
        adapter = new MessageListAdapter(getApplicationContext());
        messageListView.setAdapter(adapter);
        messageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, MessageActivity.class);
                intent.putExtra(MESSAGE, adapter.getItem(i));
                startActivity(intent);
            }
        });


        restAPI = new RestAPI(PreferenceManager.getDefaultSharedPreferences(this));
        restAPI.getMessages(new MessagesCallback() {
            @Override
            public void onTaskComplete(List<Message> messages) {
                if (messages != null) {
                    loadMessages(messages);
                } else {
                    Toast.makeText(MainActivity.this, "You have to login!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent("DUPA");
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 10 * 1000, 10 * 1000, alarmIntent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(evictionReceiver, new IntentFilter("DUPA"));
        adapter.evictMessages();
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(evictionReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_create) {
            Intent intent = new Intent(this, CreateActivity.class);
            startActivity(intent);
        }
        if (id == R.id.action_refresh) {
            restAPI.getMessages(new MessagesCallback() {
                @Override
                public void onTaskComplete(List<Message> messages) {
                    if (messages != null) {
                        loadMessages(messages);
                    } else {
                        Toast.makeText(MainActivity.this, "You have to login!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
            });

        }
        return super.onOptionsItemSelected(item);
    }

    public void setRestAPI(RestAPI restAPI) {
        this.restAPI = restAPI;
    }

    public void setAdapter(MessageListAdapter adapter) {
        this.adapter = adapter;
    }

    private void loadMessages(List<Message> messages) {
        adapter.setMessages(messages);
    }

    public void setEvictionReceiver(BroadcastReceiver evictionReceiver) {
        this.evictionReceiver = evictionReceiver;
    }

    public class EvictionReceiver extends BroadcastReceiver {

        private static final String TAG = "EvictionReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Eviction performed ");
            adapter.evictMessages();
        }
    }


}
