package com.sliwa.adrian.sdmessaging.rest;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sliwa.adrian.sdmessaging.base.BackgroundTask;
import com.sliwa.adrian.sdmessaging.base.Function;
import com.sliwa.adrian.sdmessaging.base.TaskCallback;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import common.sdm.message.Credentials;
import common.sdm.message.Envelope;
import common.sdm.message.Message;

/**
 * Created by Adiki on 06.04.14.
 */
public class RestAPI {
    private static final String TAG = RestAPI.class.toString();
    private final SharedPreferences sharedPreferences;
    private Gson gson = new Gson();


    public RestAPI(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void sendMessage(Message message, RestAPICallback restAPICallback) {
        Envelope envelope = new Envelope(message, getToken());
        String json = gson.toJson(envelope);
        BackgroundTask<String[], String> backgroundTask = new BackgroundTask<String[], String>(new Post(), restAPICallback);
        String []arguments = {getBaseUrl() + "sendMessage", json};
        Log.d(TAG, "Send message");
        backgroundTask.execute(arguments);
    }

    public void getMessages(MessagesCallback messagesCallback) {
        BackgroundTask<String[], List<Message>> backgroundTask = new BackgroundTask<String[], List<Message>>(new GetMessages(), messagesCallback);
        String []arguments = {getBaseUrl() + "getMessages/" + getLogin(), getToken()};
        backgroundTask.execute(arguments);
    }

    public void register(Credentials credentials, TaskCallback<String> taskCallback) {
        String json = gson.toJson(credentials);
        BackgroundTask<String[], String> backgroundTask = new BackgroundTask<String[], String>(new Post(), taskCallback);
        String []arguments = {getBaseUrl() + "register", json};
        backgroundTask.execute(arguments);
    }

    public void login(Credentials credentials, TaskCallback<String> taskCallback) {
        String json = gson.toJson(credentials);
        BackgroundTask<String[], String> backgroundTask = new BackgroundTask<String[], String>(new Post(), taskCallback);
        String []arguments = {getBaseUrl() + "login", json};
        backgroundTask.execute(arguments);
    }

    private String getLogin() {
        String username = sharedPreferences.getString("username", "?");
        Log.d(TAG, "username " + username);
        return username;
    }

    private String getToken() {
        String token = sharedPreferences.getString("token", "?");
        Log.d(TAG, "token " + token);
        return token;
    }

    private String getBaseUrl() {
        StringBuilder baseUrl = new StringBuilder();
        baseUrl.append("http://");
        baseUrl.append(sharedPreferences.getString("server_ip", "?"));
        baseUrl.append(":");
        baseUrl.append(sharedPreferences.getString("server_port", "?"));
        baseUrl.append("/SelfDestructableMessaging/");
        return baseUrl.toString();
    }

    private class GetMessages implements Function<String[], List<Message>> {
        @Override
        public List<Message> apply(String[] arguments) {
            String json = new Post().apply(arguments);
            if (!json.equalsIgnoreCase("")) {
                Type type = new TypeToken<List<Message>>(){}.getType();
                return gson.fromJson(json, type);
            }
            return null;
        }
    }
}
