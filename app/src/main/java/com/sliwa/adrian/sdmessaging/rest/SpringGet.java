package com.sliwa.adrian.sdmessaging.rest;

import android.util.Log;

import com.sliwa.adrian.sdmessaging.base.Function;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Adiki on 08.04.14.
 */
public class SpringGet implements Function<String, String> {
    private static final String TAG = SpringGet.class.toString();

    @Override
    public String apply(String url) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            return restTemplate.getForObject(url, String.class);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return null;
    }
}
