package com.sliwa.adrian.sdmessaging.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sliwa.adrian.sdmessaging.app.R;

import java.util.ArrayList;
import java.util.List;

import common.sdm.message.Message;

/**
 * Created by Adiki on 09.04.14.
 */
public class MessageListAdapter extends BaseAdapter {
    private final Context context;
    private List<Message> messages;

    public MessageListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return messages != null ? messages.size() : 0;
    }

    @Override
    public Message getItem(int i) {
        return messages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, viewGroup, false);
        TextView fromTextView = (TextView) rowView.findViewById(R.id.fromTextView);
        TextView topicTextView = (TextView) rowView.findViewById(R.id.topicTextView);
        TextView sentDateTextView = (TextView) rowView.findViewById(R.id.sentDateTextView);
        TextView validTextView = (TextView) rowView.findViewById(R.id.validTextView);
        Message message = messages.get(i);
        fromTextView.setText(String.format("From: %s", message.getSender()));
        topicTextView.setText(String.format("Topic: %s", message.getTopic()));
        sentDateTextView.setText(String.format("Sent date: %s", message.getSentDate()));
        validTextView.setText(String.format("Valid thorugh: %dm", message.getValidThroughInMin()));
        return rowView;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void evictMessages() {
        if (messages != null) {
            List<Message> toDelete = new ArrayList<Message>();
            for (Message message : messages) {
                if(message.isToEvict()) {
                    toDelete.add(message);
                }
            }
            messages.removeAll(toDelete);
            notifyDataSetChanged();
        }
    }
}
