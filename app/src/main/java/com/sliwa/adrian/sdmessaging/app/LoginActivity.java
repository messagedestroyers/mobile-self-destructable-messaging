package com.sliwa.adrian.sdmessaging.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sliwa.adrian.sdmessaging.rest.RestAPI;
import com.sliwa.adrian.sdmessaging.rest.RestAPICallback;

import java.lang.reflect.Field;

import common.sdm.message.Credentials;
import common.sdm.message.ResponseStatus;

public class LoginActivity extends ActionBarActivity {
    private static final String TAG = LoginActivity.class.toString();
    private SharedPreferences sharedPreferences;

    private RestAPI restAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }

        sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        restAPI = new RestAPI(sharedPreferences);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onLoginClick(View v) {
        String username = ((TextView) findViewById(R.id.usernameTextView)).getText().toString();
        String password = ((TextView) findViewById(R.id.passwordTextView)).getText().toString();
        Credentials credentials = new Credentials(username, password);
        restAPI.login(credentials, new RestAPICallback() {
            @Override
            public void onTaskComplete(String result) {
                Log.d(TAG, "Login result:" + result);
                if (result == null || result.equalsIgnoreCase("FAIL")) {
                    Toast.makeText(LoginActivity.this, "Login failed!", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor  editor = sharedPreferences.edit();
                    editor.putString("username", ((EditText) findViewById(R.id.usernameTextView)).getText().toString());
                    editor.putString("token", result);
                    editor.commit();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    public void onRegisterClick(View v) {
        String username = ((TextView) findViewById(R.id.usernameTextView)).getText().toString();
        String password = ((TextView) findViewById(R.id.passwordTextView)).getText().toString();
        Credentials credentials = new Credentials(username, password);
        restAPI.register(credentials, new RestAPICallback() {
            @Override
            public void onTaskComplete(String result) {
                Log.d(TAG, "Registartion result:" + result);
                if (result == null) {
                    Toast.makeText(LoginActivity.this, "Registration failed!", Toast.LENGTH_SHORT).show();
                } else {
                    if (result.equalsIgnoreCase("OK")) {
                        onLoginClick(null);
                    } else {
                        Toast.makeText(LoginActivity.this, "User exists!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void setRestAPI(RestAPI restAPI) {
        this.restAPI = restAPI;
    }
}
