package com.sliwa.adrian.sdmessaging.base;

/**
 * Created by Adiki on 08.04.14.
 */
public interface Function<F, T> {
    public T apply(F argument);
}
