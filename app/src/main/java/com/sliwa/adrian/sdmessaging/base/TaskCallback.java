package com.sliwa.adrian.sdmessaging.base;

public interface TaskCallback<T> {
	public void onTaskComplete(T result);
}
