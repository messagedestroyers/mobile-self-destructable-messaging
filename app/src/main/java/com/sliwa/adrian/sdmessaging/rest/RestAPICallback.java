package com.sliwa.adrian.sdmessaging.rest;

import com.sliwa.adrian.sdmessaging.base.TaskCallback;

/**
 * Created by Adiki on 08.04.14.
 */
public interface RestAPICallback extends TaskCallback<String> {
}
