package com.sliwa.adrian.sdmessaging.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sliwa.adrian.sdmessaging.rest.RestAPI;
import com.sliwa.adrian.sdmessaging.rest.RestAPICallback;

import common.sdm.message.Message;
import common.sdm.message.ResponseStatus;

public class CreateActivity extends ActionBarActivity {

    private RestAPI restAPI;
    private TextView timeTextView;
    private SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        timeTextView = (TextView)findViewById(R.id.timeTextView);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        seekBar.setMax(59);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                timeTextView.setText(String.format("%dm", i + 1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        restAPI = new RestAPI(PreferenceManager.getDefaultSharedPreferences(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_send) {
            EditText recipientTextField = (EditText) findViewById(R.id.recipientTextField);
            EditText topicTextField = (EditText) findViewById(R.id.topicTextField);
            EditText messageTextField = (EditText) findViewById(R.id.messageTextField);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            Message message = new Message(sharedPreferences.getString("username", "?"),
                                          recipientTextField.getText().toString(),
                                          topicTextField.getText().toString(),
                                          messageTextField.getText().toString(),
                                          System.currentTimeMillis(),
                                          (seekBar.getProgress() + 1) * 60 * 1000L);
            restAPI.sendMessage(message, new RestAPICallback() {
                @Override
                public void onTaskComplete(String result) {
                    if (result != null && result.equalsIgnoreCase("OK")) {
                        Toast.makeText(CreateActivity.this, "Message sent", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CreateActivity.this, "Message wasn't sent", Toast.LENGTH_SHORT).show();
                    }
                    finish();
                }
            });
            return true;
        }
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setRestAPI(RestAPI restAPI) {
        this.restAPI = restAPI;
    }

}
