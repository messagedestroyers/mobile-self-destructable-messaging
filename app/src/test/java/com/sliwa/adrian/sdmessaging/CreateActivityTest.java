package com.sliwa.adrian.sdmessaging;

import android.content.Intent;
import android.view.MenuItem;

import com.sliwa.adrian.sdmessaging.app.CreateActivity;
import com.sliwa.adrian.sdmessaging.app.R;
import com.sliwa.adrian.sdmessaging.app.SettingsActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.tester.android.view.TestMenuItem;
import org.robolectric.util.ActivityController;

import static org.junit.Assert.assertEquals;
import static org.robolectric.Robolectric.buildActivity;
import static org.robolectric.Robolectric.shadowOf;

/**
 * Created by Adiki on 2014-06-11.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class CreateActivityTest {

    private final ActivityController<CreateActivity> controller = buildActivity(CreateActivity.class);

    @Test
    public void testTrueIsTrue() throws Exception {
        CreateActivity createActivity = null;//(CreateActivity.class);

        MenuItem item = new TestMenuItem() {
            public int getItemId() {
                return R.id.action_settings;
            }
        };

        createActivity.onOptionsItemSelected(item);
        Intent expectedIntent = new Intent(createActivity, SettingsActivity.class);
        assertEquals(shadowOf(createActivity).getNextStartedActivity(), expectedIntent);
    }



}
